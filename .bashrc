# Mateo's Bash Configuration

# Prompt
PS1="\w $ "

# Vi Mode
set -o vi

# Defaults
export EDITOR="vim"
export VISUAL="$EDITOR"

# Window Swallowing
alias mpv="devour mpv"
alias sxiv="devour sxiv -b"
alias zathura="devour zathura"
alias zaread="devour zaread"
alias minecraft="devour flatpak run com.mojang.Minecraft"

# Shortcuts
alias c="clear"
alias ls="exa -l"
alias yt="ytfzf -t"
alias wefetch="curl -s wttr.in/Quito | sed -n 2,7p"
alias se="sudoedit"
alias sx="startx"

# Font "Installer"
function ifont {
    cp "$1" $HOME/.fonts/
    fc-cache -fv
}

# Void Shortcuts
alias sxi="sudo xbps-install -S"
alias sxr="sudo xbps-remove"
alias sxq="xbps-query -Rs"

# Quick Configurations
alias vc="vim ~/.vimrc"
alias bc="vim ~/.bashrc"
alias qc="vim ~/.config/qtile/config.py"

# Manage Dotfiles
alias dotfiles="/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
