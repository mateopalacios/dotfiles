" Mateo's Vim Configuration

" Terminal font: FiraCode NF

let mapleader = " "

" Fix Colorscheme Error
set termguicolors
set t_ut=""

" Fix Drawing Weird Characters
autocmd VimEnter * redraw!
set t_RV=

" Fix slowness with certain files.
set timeoutlen=1000
set ttimeoutlen=0

" Encoding
set encoding=UTF-8
set termencoding=utf-8

" Setup
scriptencoding utf-8
set backspace=2
set backspace=indent,eol,start
set mouse=a
set belloff=all " No error sounds.
set fillchars+=vert:│

" Command Line
set wildmenu " Command line autocompletition.
set ignorecase

" Editor
set laststatus=0
set nowrap
set splitright
set relativenumber
set number
set colorcolumn=79
set nofoldenable " Don't fold code blocks.

" Tabs to spaces.
set tabstop=4
set shiftwidth=4
set expandtab

" Automatic Closing
inoremap { {}<left>
inoremap {<CR> {<CR>}<ESC>O
inoremap {;<CR> {<CR>};<ESC>O

" Navigating Splits
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

inoremap jk <Esc>

" Buffer Related Mappings
nnoremap <leader><space> :wqa!<CR>
nnoremap <leader>x :bd<CR>

" Disable Arrowkeys
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>
inoremap <Up> <Nop>
inoremap <Down> <Nop>
inoremap <Left> <Nop>
inoremap <Right> <Nop>

" Auto Complete Movement
inoremap <expr> <C-j> ((pumvisible())?("\<C-n>"):("j"))
inoremap <expr> <C-k> ((pumvisible())?("\<C-p>"):("k"))

" Other Mappings
nnoremap _ ddO<esc>
" Turn the current word to uppercase.
inoremap uu <esc>vbUea
nnoremap <c-u> vbUe
" Quickly edit this config file from another session.
nnoremap <leader>vr :vsplit $MYVIMRC<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>
inoremap "" <esc>bi"<esc>ea"
inoremap '' <esc>bi'<esc>ea'

" Abbreviations
iabbrev nnll Mateo Palacios

" SpellChecking
nnoremap cn ]s1z=<cr>
nnoremap cb [s1z=<cr>

" Templates
if has ("autocmd")
    augroup templates
        autocmd BufNewFile *.c 0r ~/.vim/templates/ct.c
    augroup END
endif

set nocompatible
" Plugins
call plug#begin('~/.vim/plugged')
    " Theme
    Plug 'morhetz/gruvbox'
    " Syntax
    Plug 'vim-syntastic/syntastic'
    Plug 'Yggdroot/indentLine'
    Plug 'vim-python/python-syntax'
    " Browser
    Plug 'ctrlpvim/ctrlp.vim'
    " Other Utilities
    Plug 'sheerun/vim-polyglot'
    Plug 'ryanoasis/vim-devicons'
    Plug 'preservim/nerdcommenter'
    Plug 'chrisbra/Colorizer'
call plug#end()

" NerdCommenter
filetype plugin on
let g:NERDCreateDefaultMappings = 1
let g:NERDSpaceDelims = 1

" Colorscheme Setup
set background=dark
let g:gruvbox_bold = '0'
let g:gruvbox_contrast_dark = 'medium'
colorscheme gruvbox

" Load Colorizer
let g:colorizer_auto_filetype='css,html'
