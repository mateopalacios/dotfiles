# Mateo's Qtile Configuration

# Gruvbox ColorScheme

from typing import List

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

import os
import subprocess

mod = 'mod4'
home=os.path.expanduser('~')

class command:
    terminal = 'kitty'
    browser = 'brave-browser-stable'
    music_player = 'kitty ncspot'
    online_video_player = 'kitty ytfzf -t'
    video_conferencing = 'flatpak run us.zoom.Zoom'
    chat = 'brave-browser-stable https://discord.com/channels/@me'
    process_manager = 'kitty gotop'

    def script(script_name):
        return os.path.join(home, '.desktop-scripts/' + script_name)

keys = [
    # Switch between windows
    Key([mod], 'h', lazy.layout.left(), desc='Move focus to left.'),
    Key([mod], 'l', lazy.layout.right(), desc='Move focus to right.'),
    Key([mod], 'j', lazy.layout.down(), desc='Move focus down.'),
    Key([mod], 'k', lazy.layout.up(), desc='Move focus up.'),

    # Move windows between left/right columns or up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, 'shift'], 'h', lazy.layout.shuffle_left(),
        desc='Move window to the left.'),
    Key([mod, 'shift'], 'l', lazy.layout.shuffle_right(),
        desc='Move window to the right.'),
    Key([mod, 'shift'], 'j', lazy.layout.shuffle_down(),
        desc='Move window down.'),
    Key([mod, 'shift'], 'k', lazy.layout.shuffle_up(),\
        desc='Move window up.'),

    # Grow Windows
    Key([mod, 'control'], 'h', lazy.layout.grow_left(),
        lazy.layout.shrink_main(),
        desc='Grow window to the left.'),
    Key([mod, 'control'], 'l', lazy.layout.grow_right(),
        lazy.layout.grow_main(),
        desc='Grow window to the right.'),
    Key([mod, 'control'], 'j', lazy.layout.grow_down(),
        lazy.layout.shrink(),
        desc='Grow window down.'),
    Key([mod, 'control'], 'k', lazy.layout.grow_up(),
        lazy.layout.grow(),
        desc='Grow window up.'),
    Key([mod], 'n', lazy.layout.normalize(),
        lazy.layout.reset(),
        desc='Reset all window sizes.'),

    # Toggle Between Splits
    Key([mod, 'shift'], 'Return', lazy.layout.toggle_split(),
        desc='Toggle between split and unsplit sides of stack'),

    # Toggle Between Keyboard Layouts
    Key([mod], "space", lazy.widget["keyboardlayout"].next_keyboard(),
        desc="Next keyboard layout."),

    # Terminal
    Key([mod], 'Return', lazy.spawn(command.terminal),
        desc='Launch terminal.'),

    # Toggle Between Layouts
    Key([mod], 'Tab', lazy.next_layout(), desc='Toggle between layouts'),
    Key([mod], 'x', lazy.window.kill(), desc='Kill focused window.'),

    # Qtile
    Key([mod, 'control'], 'r', lazy.restart(), desc='Restart Qtile.'),
    Key([mod, 'control'], 'q', lazy.shutdown(), desc='Shutdown Qtile.'),

    # Running Commands
    Key([mod], 'r', lazy.spawncmd(),
        desc='Spawn a command using a prompt widget.'),

    # Brightness
    Key([], 'XF86MonBrightnessUp', lazy.spawn('xbacklight +5'),
        desc='Increase brightness.'),
    Key([], 'XF86MonBrightnessDown', lazy.spawn('xbacklight -5'),
        desc='Decrease brightness.'),

    # Screenshots
    Key([], 'Print', lazy.spawn(command.script('screenshots save')),
        desc='Take screenshot.'),
    Key(['control'], 'Print', lazy.spawn(command.script('screenshots copy')),
        desc='Take screenshot and copy it to the clipboard.'),
    Key(['shift'], 'Print', lazy.spawn(command.script('screenshots window')),
        desc='Screenshot the active window and copy it to the clipboard.'),

    # Audio
    Key([], 'XF86AudioRaiseVolume', lazy.spawn(command.script\
        ('audio-ctrl raise 3')),
        desc='Increase volume.'),
    Key([], 'XF86AudioLowerVolume', lazy.spawn(command.script\
        ('audio-ctrl lower 3')), desc='Decrease volume.'),
    Key([], 'XF86AudioMute', lazy.spawn(command.script('audio-ctrl toggle')),
        desc='Toggle audio card.'),

    # Programs
    Key([mod], 'b', lazy.spawn(command.browser), desc='Opens the\
        browser.'),
    Key([mod], 'm', lazy.spawn(command.music_player), desc=\
        'Opens Spotify.'),
    Key([mod], 'y', lazy.spawn(command.online_video_player), desc='Opens\
        Ytfzf, a terminal Youtube client.'),
    Key([mod], 'z', lazy.spawn(command.video_conferencing), desc='Opens\
        Zoom.'),
    Key([mod], 'd', lazy.spawn(command.chat), desc='Opens Discord in the\
        browser.'),
    Key([mod], 'p', lazy.spawn(command.process_manager),
        desc='Opens the process manager.'),
]

# Groups
# From: https://github.com/AugustoNicola/dotfiles/blob/main/qtile/
if __name__ in ['config', '__main__']:
    group_props = [
        ('Web', {'label':''}), ('Coding', {'label':''}),
        ('Classes', {'label':''}), ('Music', {'label':''}),
        ('Gaming', {'label':''})
    ]

    groups = [Group(name, init=True, persist=True, **kwargs) for name, kwargs\
             in group_props]

    for i, (name, kwargs) in enumerate(group_props, 1):
        keys.extend([
            # Switch to group
            Key([mod], str(i), lazy.group[name].toscreen(),
                desc='Switch to group {}'.format(name)),

            # Move window to group
            Key([mod, 'shift'], str(i), lazy.window.togroup(name,\
                switch_group=True),
                desc='Switch to & move focused window to group {}.'.\
                format(name)),
        ])

# Layouts
layouts = [
    layout.MonadTall(border_width=3,
                     border_focus='#928374',
                     border_normal='#3C3836',
                     single_border_width=3,
                     margin=0),
    layout.Max()
]

widget_defaults = dict(
    font='FiraCode Nerd Font Medium',
    fontsize=18,
    padding=3,
)
extension_defaults = widget_defaults.copy()

def widget_script(script_name, f_size, update, w_padding):
    return widget.GenPollText(foreground='#EBDBB2',
        fontsize=f_size, update_interval=update, padding=w_padding,
        func=lambda:subprocess.check_output(command.script(script_name)).\
        decode("utf-8"))

screens = [
    Screen(
            # Bar
            top=bar.Bar(
            [
                # Layout
                widget.Spacer(4),
                widget.CurrentLayoutIcon(custom_icon_paths = [os.path.\
                                         expanduser("~/.config/qtile/icons")],
                                         padding=0,
                                         scale=0.45),
                widget.Spacer(3),
                # GroupBox
                widget.GroupBox(padding=6,
                                fontsize=27,
                                spacing=10,
                                block_highlight_text_color='#EBDBB2',
                                active='#928374',
                                inactive='#504945',
                                borderwidth=0),
                widget.Spacer(10),
                # Spawn
                widget.Prompt(prompt=': ',
                              cursor_color='#929AA9',
                              foreground='#928374'),
                # Song Name
                widget_script('song-name', 18, 1, 4),
                widget.Spacer(length=bar.STRETCH),
                # Systray
                widget.Systray(icon_size=20,
                               padding=18),
                widget.Spacer(16),
                # Keyboard
                widget.KeyboardLayout(configured_keyboards=['us', 'es'],
                                      foreground='#EBDBB2'),
                widget.Spacer(10),
                # Battery
                widget_script('battery', 18, 1, 4),
                widget.Battery(foreground='#EBDBB2',
                               low_foreground='#FB4934',
                               notify_below=10,
                               update_interval=1,
                               show_short_text=False,
                               format='{percent:2.0%}'),
                widget.Spacer(10),
                # Backlight
                widget.TextBox(text='盛',
                               padding=6,
                               foreground='#EBDBB2'),
                widget.Backlight(foreground='#EBDBB2',
                                 backlight_name='intel_backlight',
                                 change_command='xbacklight -set {0}'),
                widget.Spacer(10),
                # Volume
                widget.TextBox(text='墳',
                               foreground='#EBDBB2',
                               padding=6),
                widget_script('volume', 18, 0.1500, 2),
                widget.Spacer(10),
                # Time
                widget.TextBox(text='',
                               padding=6,
                               foreground='#EBDBB2'),
                widget.Clock(foreground='#EBDBB2',
                             format='%I:%M %p'),
                widget.Spacer(8),
                # Wifi
                widget_script('wifi', 18, 1, 6),
                widget.Spacer(14)
            ],
            48,
            background='#282828',
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], 'Button1', lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], 'Button3', lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], 'Button2', lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    ],
    # Configuration
    border_width=3,
    border_focus='#928374',
    border_normal='#3C3836')
auto_fullscreen = True
focus_on_window_activation = 'smart'

# Java UI ToolKits
wmname = 'LG3D'
